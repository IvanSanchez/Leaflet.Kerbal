
# Leaflet.Kerbal

A [LeafletJS](http://www.leafletjs.com) plugin to display tiles from [http://www.kerbalmaps.com](http://www.kerbalmaps.com).

## Demo

[http://ivansanchez.gitlab.io/Leaflet.Kerbal/demo.html](http://ivansanchez.gitlab.io/Leaflet.Kerbal/demo.html)

## Why?

This is mostly an attempt to upgrade the functionality from [Joel Pedraza's Leaflet.KSP](https://github.com/saik0/Leaflet.KSP) in order to use the features of Leaflet 1.0.0.

This means things like infinite horizontal scrolling and out-of-the-box distance calculations are possible.

## Usage

This should have a build system and some kind of CDN in the future. Until then, develop from source and include all the files:

```
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.0-rc.3/dist/leaflet.css" />
<script src="https://unpkg.com/leaflet@1.0.0-rc.3/dist/leaflet.js"></script>
<script src="src/crs.js"></script>
<script src="src/tile.js"></script>
```

So far, this adds equirectangular CRSs for Kerbin, the Mün and Minmus:

```
var map = L.map('map-container', {
	crs: L.CRS.Kerbin.Equirectangular
});
```

And tile layers which use `tiles.kerbalmaps.com` as a backend, which work with the equirectangular CRSs:

```
L.tileLayer.kerbalMaps({
	// Name of the celestial body (in lowercase)
	body: 'kerbin',

	// Raster style. One of 'sat' (visible RGB), 'color' (terrain colour relief), 'slope' or 'biome'
	style: 'sat',
}).addTo(map);
```

## Legalese

----------------------------------------------------------------------------

"THE BEER-WARE LICENSE":
<ivan@sanchezortega.es> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return.

----------------------------------------------------------------------------

